#
# About
#

A small RESTful backend for a workshift system.

The system can:

- Create a user
- Create a shop
- Add a user to a shop
- Create a shift
- Add a user to a shift at a shop

Rules:

- A user can work in different shops
- No user is allowed to work in the same shop for more than 8 hours, within a 24 hour window.
- No user can work more than 5 days in a row in the same shop.
- A user can not work in multiple shops at the same time.

#
# How to build and run
#

1) Requires maven and Java 17 or later to be installed.
2) Build the spring boot server: mvn package
3) Run the spring boot server: java -jar target/workshift-1.0.0.jar
4) Tryout the rest api at: localhost:8080/