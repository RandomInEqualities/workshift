package workshift;

import java.time.Duration;

/**
 * General constants that could be moved to a configuration file.
 */
public final class ApplicationConstants
{
    /**
     * How many days that a user is at most allowed to work in the same shop for.
     */
    public static final int MAX_CONSECUTIVE_WORK_DAYS_IN_SAME_SHOP = 5;

    /**
     * How long that a user is allowed to work for during a work time window.
     */
    public static final Duration MAX_CONSECUTIVE_WORK_TIME = Duration.ofHours(8);
    public static final Duration MAX_CONSECUTIVE_WORK_TIME_WINDOW = Duration.ofHours(24);

    /**
     * The maximum name length of users and shops.
     */
    public static final int MAX_USER_AND_SHOP_NAME_LENGTH = 50;
}
