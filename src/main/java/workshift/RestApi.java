package workshift;

import jakarta.persistence.EntityManager;
import jakarta.persistence.LockModeType;
import jakarta.persistence.OptimisticLockException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import workshift.database.Employment;
import workshift.database.EmploymentRepository;
import workshift.database.Shift;
import workshift.database.ShiftRepository;
import workshift.database.Shop;
import workshift.database.ShopRepository;
import workshift.database.User;
import workshift.database.UserRepository;
import workshift.exceptions.NotFoundException;
import workshift.services.AssignmentService;
import workshift.services.CreationService;
import workshift.utilities.DateTimeRange;
import java.time.LocalDateTime;

/**
 * Implements the rest api of the application allowing the creation of users, shops and shifts.
 */
@RestController
public class RestApi
{
    /**
     * Get users in the application.
     *
     * Example usage: GET /users?page=0&size=10&sort=name
     *
     * @param pageable Allows paged access to users.
     * @return The users as a json array.
     */
    @GetMapping("/users")
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public Page<User> users(final Pageable pageable)
    {
        return userRepository.findAll(pageable);
    }

    /**
     * Create a user in the application.
     *
     * Example usage: POST /users?name=bob
     *
     * @param name The name of the user. Must not be longer than 50 characters.
     * @return The created user as a json object.
     */
    @PostMapping("/users")
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public User createUser(@RequestParam String name)
    {
        return creationService.createUser(name);
    }

    /**
     * Get shops in the application.
     *
     * Example usage: GET /shops?page=0&size=10&sort=name
     *
     * @param pageable Allows paged access to shops.
     * @return The shops as a json array.
     */
    @GetMapping("/shops")
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public Page<Shop> shops(final Pageable pageable)
    {
        return shopRepository.findAll(pageable);
    }

    /**
     * Create a shop in the application.
     *
     * Example usage: POST /shops?name=Walmart
     *
     * @param name The name of the shop. Must not be longer than 50 characters.
     * @return The created shop as a json object.
     */
    @PostMapping("/shops")
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Shop createShop(@RequestParam String name)
    {
        return creationService.createShop(name);
    }

    /**
     * Get which users works in which shops.
     *
     * Example usage: GET /employments?page=0&size=10
     *
     * @param pageable Allows paged access to employments.
     * @return The employments as a json array.
     */
    @GetMapping("/employments")
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public Page<Employment> employments(final Pageable pageable)
    {
        return employmentRepository.findAll(pageable);
    }

    /**
     * Set a user as employed in a shop. Allowing the user to take shifts.
     *
     * Example usage: POST /shops/1/users/1
     *
     * @param shopId The id of the shop.
     * @param userId The id of the user to employ.
     * @return The created employment as a json object.
     */
    @PostMapping("/shops/{shopId}/users/{userId}")
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Employment createEmployment(@PathVariable Long shopId, @PathVariable Long userId)
    {
        final Shop shop = findAndLockEntityById(shopId, shopRepository, "Shop");
        final User user = findAndLockEntityById(userId, userRepository, "User");
        return creationService.createEmployment(user, shop);
    }

    /**
     * Get shifts in the application (shifts in shops that users can take).
     *
     * Example usage: GET /shifts?page=0&size=10
     *
     * @param pageable Allows paged access to shifts.
     * @return The shifts as a json array.
     */
    @GetMapping("/shifts")
    @Transactional(isolation = Isolation.READ_COMMITTED, readOnly = true)
    public Page<Shift> shifts(final Pageable pageable)
    {
        return shiftRepository.findAll(pageable);
    }

    /**
     * Create a shift for a shop. Allowing an employed user to be assigned to the shift.
     *
     * Example usage: POST /shops/1/shifts?startTime=2007-12-03T10:00:00&endTime=2007-12-03T18:00:00
     *
     * @param shopId The id of the shop.
     * @param startTime When the shift starts in the ISO-8601 calendar system. Must be UTC time without timezone information.
     * @param endTime When the shift ends in the ISO-8601 calendar system. Must be UTC time without timezone information.
     * @return The created shift as a json object.
     */
    @PostMapping("/shops/{shopId}/shifts")
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Shift createShift(@PathVariable Long shopId, @RequestParam String startTime, @RequestParam String endTime)
    {
        final Shop shop = findAndLockEntityById(shopId, shopRepository, "Shop");
        final DateTimeRange shiftTime = new DateTimeRange(LocalDateTime.parse(startTime), LocalDateTime.parse(endTime));
        return creationService.createShift(shop, shiftTime);
    }

    /**
     * Set a user as assigned to a shift, removing the user that was previously assigned.
     *
     * Example usage: PATCH /shifts/1/users/1
     *
     * Rules:
     * - A user can not work in multiple shops at the same time.
     * - No user is allowed to work in the same shop for more than 8 hours, within a 24-hour window.
     * - No user can work more than 5 days in a row in the same shop.
     *
     * @param shiftId The id of the shift to assign the user to.
     * @param userId The id of the user to assign to the shift.
     * @return The updated shift as a json object.
     */
    @PatchMapping("/shifts/{shiftId}/users/{userId}")
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public Shift assignUserToShift(@PathVariable Long shiftId, @PathVariable Long userId)
    {
        final Shift shift = findAndLockEntityById(shiftId, shiftRepository, "Shift");
        final User user = findAndLockEntityById(userId, userRepository, "User");
        assignmentService.assignUserToShift(shift, user);
        return shift;
    }

    /**
     * Find an entity by its id and lock it for concurrent access.
     *
     * Allows safe concurrent access for the duration of the current transaction.
     *
     * @throws NotFoundException if the entity cannot be found or does not exist.
     * @throws OptimisticLockException if a concurrent process already has the entity locked.
     */
    private <T> T findAndLockEntityById(final Long id, final JpaRepository<T, Long> repository, final String entityName)
    {
        final T entity = repository.findById(id).orElseThrow(() -> new NotFoundException(entityName + " id is not valid."));
        entityManager.lock(entity, LockModeType.PESSIMISTIC_FORCE_INCREMENT);
        return entity;
    }

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private EmploymentRepository employmentRepository;
    @Autowired
    private ShiftRepository shiftRepository;
    @Autowired
    private CreationService creationService;
    @Autowired
    private AssignmentService assignmentService;
    @Autowired
    private EntityManager entityManager;
}
