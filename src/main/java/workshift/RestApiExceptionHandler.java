package workshift;

import jakarta.persistence.OptimisticLockException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import workshift.exceptions.NotFoundException;
import workshift.exceptions.ServiceException;
import java.time.format.DateTimeParseException;

/**
 * Class that implements custom error messages for exceptions that are thrown in the rest API.
 */
@RestControllerAdvice
public final class RestApiExceptionHandler extends ResponseEntityExceptionHandler
{
    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<Object> handleException(final ServiceException e)
    {
        return createResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleException(final NotFoundException e)
    {
        return createResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ExceptionHandler(DateTimeParseException.class)
    public ResponseEntity<Object> handleException(final DateTimeParseException e)
    {
        return createResponse(HttpStatus.BAD_REQUEST, "Invalid date format.");
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Object> handleException(final MethodArgumentTypeMismatchException e)
    {
        return createResponse(HttpStatus.BAD_REQUEST, "Invalid parameter format: " + e.getName());
    }

    @ExceptionHandler(OptimisticLockException.class)
    public ResponseEntity<Object> handleException(final OptimisticLockException e)
    {
        return createResponse(HttpStatus.BAD_REQUEST, "Another update is in progress.");
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(final Exception e)
    {
        return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error.");
    }

    private static ResponseEntity<Object> createResponse(final HttpStatus status, final String message)
    {
        return new ResponseEntity<>(message, status);
    }
}
