package workshift.database;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represents an employment between a user and a shop.
 */
@Entity
@Table(name="Employments")
@NoArgsConstructor
public class Employment
{
    /**
     * Create an employment between a user and a shop.
     */
    public Employment(final User user, final Shop shop)
    {
        this.user = user;
        this.shop = shop;
    }

    /**
     * The id for each employment in the database.
     */
    @Id
    @GeneratedValue
    @NotNull
    @Getter
    private long id;

    /**
     * The user that the employment is about.
     */
    @ManyToOne
    @NotNull
    @Getter
    private User user;

    /**
     * The shop that the employment is about.
     */
    @ManyToOne
    @NotNull
    @Getter
    private Shop shop;
}
