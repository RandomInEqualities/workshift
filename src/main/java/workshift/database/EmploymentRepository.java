package workshift.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Repository that allows access to the employments table in the backing database.
 */
public interface EmploymentRepository extends JpaRepository<Employment, Long>
{
    /**
     * Check how many times that the given user is employed at the given shop (either 0 or 1).
     */
    @Query("SELECT COUNT(e) FROM Employment e WHERE e.user.id = ?1 AND e.shop.id = ?2")
    long countEmploymentsByUserInShop(long userId, long shopId);
}
