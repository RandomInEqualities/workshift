package workshift.database;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import workshift.utilities.DateTimeRange;

/**
 * Represents a shift in a shop that a user can be assigned to.
 */
@Entity
@Table(name="Shifts")
@NoArgsConstructor
public class Shift
{
    /**
     * Create a shift for a shop between two times.
     */
    public Shift(final Shop shop, final DateTimeRange time)
    {
        this.shop = shop;
        this.time = time;
    }

    /**
     * The id for each shift in the database.
     */
    @Id
    @GeneratedValue
    @NotNull
    @Getter
    private long id;

    /**
     * The shop that the shift is assigned to.
     */
    @ManyToOne
    @NotNull
    @Getter
    private Shop shop;

    /**
     * The time of the shift (start time and end time).
     */
    @Embedded
    @NotNull
    @Getter
    private DateTimeRange time;

    /**
     * The user that is assigned to the shift (can be null).
     */
    @ManyToOne
    @Getter
    @Setter
    private User user;

    /**
     * Version incremented on each concurrent access.
     */
    @Version
    @NotNull
    private long version;
}
