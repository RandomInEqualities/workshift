package workshift.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import workshift.utilities.DateTimeRange;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Repository that allows access to the shifts table in the backing database.
 */
public interface ShiftRepository extends JpaRepository<Shift, Long>
{
    /**
     * Count how many shifts that the given user is assigned to between the given times (excludes the ends of the range).
     */
    @Query("SELECT COUNT(s) FROM Shift s WHERE s.user.id = ?1 AND (?2 < s.time.endTime AND s.time.startTime < ?3) AND s.shop.id != ?4")
    long countShiftsByUserWithinTime(long userId, LocalDateTime startTime, LocalDateTime endTime, long excludedShopId);

    /**
     * Get the shifts that the user is assigned to between the given times (excludes the ends of the range).
     */
    @Query("SELECT s.time FROM Shift s WHERE s.user.id = ?1 AND (?2 < s.time.endTime AND s.time.startTime < ?3)")
    List<DateTimeRange> getShiftTimesByUserWithinTime(long userId, LocalDateTime startTime, LocalDateTime endTime);

    /**
     * Get the shifts that the user is assigned to in the given shop between the given times (excludes the ends of the range).
     */
    @Query("SELECT s.time FROM Shift s WHERE s.user.id = ?1 AND s.shop.id = ?2 AND (?3 < s.time.endTime AND s.time.startTime < ?4)")
    List<DateTimeRange> getShiftTimesByUserAndShopWithinTime(long userId, long shopId, LocalDateTime startTime, LocalDateTime endTime);
}
