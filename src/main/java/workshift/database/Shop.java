package workshift.database;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represents a shop that a user can be employed in.
 */
@Entity
@Table(name="Shops")
@NoArgsConstructor
public class Shop
{
    /**
     * Create a shop with a specific name.
     */
    public Shop(final String name)
    {
        this.name = name;
    }

    /**
     * The id for each shop in the database.
     */
    @Id
    @GeneratedValue
    @NotNull
    @Getter
    private long id;

    /**
     * The name of the shop (user supplied).
     */
    @NotNull
    @NotBlank
    @Getter
    private String name;

    /**
     * Version incremented on each concurrent access.
     */
    @Version
    @NotNull
    private long version;
}
