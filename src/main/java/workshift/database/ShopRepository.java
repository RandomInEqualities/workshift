package workshift.database;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository that allows access to the shops table in the backing database.
 */
public interface ShopRepository extends JpaRepository<Shop, Long>
{
}
