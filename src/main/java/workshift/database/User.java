package workshift.database;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represents a user that can be assigned to stuff.
 */
@Entity
@Table(name="Users")
@NoArgsConstructor
public class User
{
    /**
     * Create a user with a specific name.
     */
    public User(final String name)
    {
        this.name = name;
    }

    /**
     * The id for each user in the database.
     */
    @Id
    @GeneratedValue
    @NotNull
    @Getter
    private long id;

    /**
     * The name of the user (user supplied).
     */
    @NotNull
    @NotBlank
    @Getter
    private String name;

    /**
     * Version incremented on each concurrent access.
     */
    @Version
    @NotNull
    private long version;
}
