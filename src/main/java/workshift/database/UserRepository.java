package workshift.database;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository that allows access to the users table in the backing database.
 */
public interface UserRepository extends JpaRepository<User, Long>
{
}
