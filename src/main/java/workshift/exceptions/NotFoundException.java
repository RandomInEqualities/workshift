package workshift.exceptions;

/**
 * Exception thrown when a resource is not found.
 */
public final class NotFoundException extends RuntimeException
{
    public NotFoundException(final String message)
    {
        super(message);
    }
}