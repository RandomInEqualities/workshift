package workshift.exceptions;

/**
 * Exception thrown when there is an error in a service.
 */
public final class ServiceException extends RuntimeException
{
    public ServiceException(final String message)
    {
        super(message);
    }
}