package workshift.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import workshift.ApplicationConstants;
import workshift.database.EmploymentRepository;
import workshift.database.Shift;
import workshift.database.ShiftRepository;
import workshift.database.Shop;
import workshift.database.User;
import workshift.exceptions.ServiceException;
import workshift.utilities.DateTimeRange;
import workshift.utilities.TimeUtilities;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Service that allows assigning users to shifts.
 */
@Service
public class AssignmentService
{
    /**
     * Assign a user to a shift.
     */
    public void assignUserToShift(final Shift shift, final User user)
    {
        final DateTimeRange time = shift.getTime();
        final Shop shop = shift.getShop();

        if (employmentRepository.countEmploymentsByUserInShop(user.getId(), shop.getId()) <= 0)
        {
            throw new ServiceException("User is not employed in the shop.");
        }
        if (shiftRepository.countShiftsByUserWithinTime(user.getId(), time.startTime(), time.endTime(), shop.getId()) > 0)
        {
            throw new ServiceException("Cannot work in multiple shops at the same time.");
        }
        if (doesUserGetTooManyConsecutiveDaysInShop(user, shop, time))
        {
            throw new ServiceException("Cannot work in the same shop 5 days in a row.");
        }
        if (doesUserGetTooManyConsecutiveWorkHours(user, time))
        {
            throw new ServiceException("Cannot work for more than 8 hours in the same shop.");
        }

        shift.setUser(user);
        shiftRepository.save(shift);
    }

    private boolean doesUserGetTooManyConsecutiveDaysInShop(final User user, final Shop shop, final DateTimeRange newShiftTime)
    {
        // Find the time ranges where the user will be working in the shop around the time of the shift.
        // Add one day to the check start and end times, to find all day-overlapping shifts.
        final int maxAllowedConsecutiveDays = ApplicationConstants.MAX_CONSECUTIVE_WORK_DAYS_IN_SAME_SHOP;
        final int dateRangeRoundingAdjustment = 1;
        final LocalDateTime checkStartTime = newShiftTime.startTime().minusDays(maxAllowedConsecutiveDays + dateRangeRoundingAdjustment);
        final LocalDateTime checkEndTime = newShiftTime.endTime().plusDays(maxAllowedConsecutiveDays + dateRangeRoundingAdjustment);
        final List<DateTimeRange> shiftTimesToCheck = shiftRepository.getShiftTimesByUserAndShopWithinTime(user.getId(), shop.getId(), checkStartTime, checkEndTime);
        shiftTimesToCheck.add(newShiftTime);

        // Check if it is possible to find more than the allowed consecutive days.
        final int longestConsecutiveDays = TimeUtilities.findLongestConsecutiveDaysInRanges(shiftTimesToCheck, checkStartTime.toLocalDate(), checkEndTime.toLocalDate());
        return longestConsecutiveDays > maxAllowedConsecutiveDays;
    }

    private boolean doesUserGetTooManyConsecutiveWorkHours(final User user, final DateTimeRange newShiftTime)
    {
        // Find the time ranges where the user will be working around the time of the shift.
        final Duration workTimeCheckWindow = ApplicationConstants.MAX_CONSECUTIVE_WORK_TIME_WINDOW;
        final LocalDateTime checkStartTime = newShiftTime.startTime().minus(workTimeCheckWindow);
        final LocalDateTime checkEndTime = newShiftTime.endTime().plus(workTimeCheckWindow);
        final List<DateTimeRange> shiftTimesToCheck = shiftRepository.getShiftTimesByUserWithinTime(user.getId(), checkStartTime, checkEndTime);
        shiftTimesToCheck.add(newShiftTime);

        // Check that the new shift does not cause the user to work more than allowed.
        final Duration longestDuration = TimeUtilities.findLongestDurationWithinMovingWindow(shiftTimesToCheck, workTimeCheckWindow);
        return longestDuration.compareTo(ApplicationConstants.MAX_CONSECUTIVE_WORK_TIME) > 0;
    }

    @Autowired
    private EmploymentRepository employmentRepository;
    @Autowired
    private ShiftRepository shiftRepository;
}
