package workshift.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import workshift.ApplicationConstants;
import workshift.database.Employment;
import workshift.database.EmploymentRepository;
import workshift.database.Shift;
import workshift.database.ShiftRepository;
import workshift.database.Shop;
import workshift.database.ShopRepository;
import workshift.database.User;
import workshift.database.UserRepository;
import workshift.exceptions.ServiceException;
import workshift.utilities.DateTimeRange;

/**
 * Service that creates different things.
 */
@Service
public class CreationService
{
    /**
     * Create a user from the given name.
     */
    public User createUser(final String name)
    {
        if (!isAppropriateShopOrUserName(name))
        {
            throw new ServiceException("Name is invalid.");
        }

        final User user = new User(name);
        userRepository.save(user);
        return user;
    }

    /**
     * Create a shop from the given name.
     */
    public Shop createShop(final String name)
    {
        if (!isAppropriateShopOrUserName(name))
        {
            throw new ServiceException("Name is invalid.");
        }

        final Shop shop = new Shop(name);
        shopRepository.save(shop);
        return shop;
    }

    /**
     * Create an employment between the given user and shop.
     */
    public Employment createEmployment(final User user, final Shop shop)
    {
        if (employmentRepository.countEmploymentsByUserInShop(user.getId(), shop.getId()) >= 1)
        {
            throw new ServiceException("User is already employed in shop.");
        }

        final Employment employment = new Employment(user, shop);
        employmentRepository.save(employment);
        return employment;
    }

    /**
     * Create a shift in the given shop.
     */
    public Shift createShift(final Shop shop, final DateTimeRange shiftTime)
    {
        if (shiftTime.endTime().isBefore(shiftTime.startTime()))
        {
            throw new ServiceException("End time cannot be before start time.");
        }
        if (shiftTime.duration().compareTo(ApplicationConstants.MAX_CONSECUTIVE_WORK_TIME) > 0)
        {
            throw new ServiceException("It is not allowed to have shifts longer than 8 hours.");
        }

        final Shift shift = new Shift(shop, shiftTime);
        shiftRepository.save(shift);
        return shift;
    }

    /**
     * Check if the given string can be used as a name for shops or users.
     */
    static boolean isAppropriateShopOrUserName(final String name)
    {
        if (name == null || name.isBlank())
        {
            return false;
        }
        if (name.length() > ApplicationConstants.MAX_USER_AND_SHOP_NAME_LENGTH)
        {
            return false;
        }
        return true;
    }

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private ShiftRepository shiftRepository;
    @Autowired
    private EmploymentRepository employmentRepository;
}
