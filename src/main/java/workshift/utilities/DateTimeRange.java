package workshift.utilities;

import jakarta.persistence.Embeddable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * A range between two date times. For a simpler workflow when using time ranges.
 */
@Embeddable
public record DateTimeRange(LocalDateTime startTime, LocalDateTime endTime)
{
    /**
     * Get how long that the range is.
     */
    public Duration duration()
    {
        return Duration.between(startTime, endTime);
    }

    /**
     * Check if this range contains the given date (inclusive).
     *
     * @return true if the given date is inside this range, including if the date is equal to the start or end date.
     */
    public boolean contains(final LocalDate date)
    {
        final LocalDate startDate = startTime.toLocalDate();
        final LocalDate endDate = endTime.toLocalDate();
        return date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0;
    }

    /**
     * Check if this range contains the given date time (inclusive).
     *
     * @return true if the given time is inside this range, including if the date is equal to the start or end time.
     */
    public boolean contains(final LocalDateTime dateTime)
    {
        return dateTime.compareTo(startTime) >= 0 && dateTime.compareTo(endTime) <= 0;
    }

    /**
     * Check if this range overlaps the given range (inclusive).
     *
     * @return true if this range overlaps with the given range, including if the start or end times are equal.
     */
    public boolean overlaps(final DateTimeRange range)
    {
        return range.endTime.compareTo(startTime) >= 0 && range.startTime.compareTo(endTime) <= 0;
    }

    /**
     * Expand this range to encompass the given range, if needed.
     */
    public DateTimeRange expandTo(final DateTimeRange range)
    {
        final LocalDateTime newStartTime = range.startTime.isBefore(startTime) ? range.startTime : startTime;
        final LocalDateTime newEndTime = range.endTime.isAfter(endTime) ? range.endTime : endTime;
        return new DateTimeRange(newStartTime, newEndTime);
    }
}
