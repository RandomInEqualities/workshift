package workshift.utilities;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalInt;

/**
 * Utility functions for working with dates and times.
 */
public final class TimeUtilities
{
    /**
     * Get the longest amount of consecutive days in the given time ranges. Searching from start date to end date.
     */
    public static int findLongestConsecutiveDaysInRanges(final List<DateTimeRange> searchRanges, final LocalDate searchStartDate, final LocalDate searchEndDate)
    {
        int currentConsecutiveDays = 0;
        int longestConsecutiveDays = 0;
        for (LocalDate searchDate = searchStartDate; !searchDate.isAfter(searchEndDate); searchDate = searchDate.plusDays(1))
        {
            // This is an O(Days*RangesSize) loop, which in this case should be fine as we will at most loop over 10 days.
            if (doesOneRangeMatchDate(searchRanges, searchDate))
            {
                currentConsecutiveDays++;
            }
            else
            {
                currentConsecutiveDays = 0;
            }

            longestConsecutiveDays = Math.max(currentConsecutiveDays, longestConsecutiveDays);
        }
        return longestConsecutiveDays;
    }

    /**
     * Finds within a time window that moves across the timeline, the duration where most time is inside the time ranges.
     */
    public static Duration findLongestDurationWithinMovingWindow(final List<DateTimeRange> searchRanges, final Duration MovingWindowLength)
    {
        // Sort and combine the ranges such that they go from first to last as a disconnected set.
        final List<DateTimeRange> ranges = combineAndSortOverlappingRanges(searchRanges);
        if (ranges.isEmpty())
        {
            return Duration.ZERO;
        }

        // Set up a time window that can run over the ranges from the first range to the last one.
        final LocalDateTime rangeStartTime = ranges.get(0).startTime();
        final LocalDateTime rangeEndTime = ranges.get(ranges.size() - 1).endTime();
        LocalDateTime windowStartTime = rangeStartTime.minus(MovingWindowLength);
        LocalDateTime windowEndTime = rangeStartTime;
        int indexOfRangeOverlappingWindowStart = -1;
        int indexOfRangeOverlappingWindowEnd = 0;
        Duration windowDurationInsideRanges = Duration.ZERO;
        Duration largestDurationInsideRanges = Duration.ZERO;

        // The algorithm is finished when the window has moved fully past the end time.
        while (!windowStartTime.isAfter(rangeEndTime))
        {
            // Advance time with a specific interval. The interval can be increased for speed or decreased for accuracy.
            final Duration advancePerStep = Duration.ofMinutes(15);
            windowStartTime = windowStartTime.plus(advancePerStep);
            windowEndTime = windowEndTime.plus(advancePerStep);

            // Detect if entering or leaving ranges while updating the durations and the overlapping range indices.
            if (containsOverlapAtIndex(ranges, indexOfRangeOverlappingWindowStart, windowStartTime))
            {
                windowDurationInsideRanges = windowDurationInsideRanges.minus(advancePerStep);
            }
            else if (containsOverlapAtIndex(ranges, indexOfRangeOverlappingWindowStart + 1, windowStartTime))
            {
                indexOfRangeOverlappingWindowStart = indexOfRangeOverlappingWindowStart + 1;
            }

            if (containsOverlapAtIndex(ranges, indexOfRangeOverlappingWindowEnd, windowEndTime))
            {
                windowDurationInsideRanges = windowDurationInsideRanges.plus(advancePerStep);
            }
            else if (containsOverlapAtIndex(ranges, indexOfRangeOverlappingWindowEnd + 1, windowEndTime))
            {
                indexOfRangeOverlappingWindowEnd = indexOfRangeOverlappingWindowEnd + 1;
            }

            // It is possible that this window range contains the largest duration in ranges.
            if (windowDurationInsideRanges.compareTo(largestDurationInsideRanges) > 0)
            {
                largestDurationInsideRanges = windowDurationInsideRanges;
            }
        }
        return largestDurationInsideRanges;
    }

    private static boolean doesOneRangeMatchDate(final List<DateTimeRange> ranges, final LocalDate date)
    {
        for (final DateTimeRange range : ranges)
        {
            if (range.contains(date))
            {
                return true;
            }
        }
        return false;
    }

    private static List<DateTimeRange> combineAndSortOverlappingRanges(final List<DateTimeRange> ranges)
    {
        // Assumes that the list is small (less than 10 ranges) so a O(RangeSize^2) loop is fast.
        List<DateTimeRange> combinedRanges = new ArrayList<>();
        for (final DateTimeRange range : ranges)
        {
            final OptionalInt indexOfRangeToCombineWith = findOverlappingRange(combinedRanges, range);
            if (indexOfRangeToCombineWith.isPresent())
            {
                final DateTimeRange rangeToCombineWith = combinedRanges.get(indexOfRangeToCombineWith.getAsInt());
                combinedRanges.set(indexOfRangeToCombineWith.getAsInt(), rangeToCombineWith.expandTo(range));
            }
            else
            {
                combinedRanges.add(range);
            }
        }

        combinedRanges.sort(Comparator.comparing(DateTimeRange::startTime));
        return combinedRanges;
    }

    private static OptionalInt findOverlappingRange(final List<DateTimeRange> ranges, final DateTimeRange rangeToFind)
    {
        for (int index = 0; index < ranges.size(); index++)
        {
            if (ranges.get(index).overlaps(rangeToFind))
            {
                return OptionalInt.of(index);
            }
        }
        return OptionalInt.empty();
    }

    private static boolean containsOverlapAtIndex(final List<DateTimeRange> ranges, final int rangeIndex, final LocalDateTime time)
    {
        if (rangeIndex >= 0 && rangeIndex < ranges.size())
        {
            final DateTimeRange range = ranges.get(rangeIndex);
            return range.contains(time);
        }
        return false;
    }
}
