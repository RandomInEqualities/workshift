package workshift;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import workshift.database.Employment;
import workshift.database.EmploymentRepository;
import workshift.database.Shift;
import workshift.database.ShiftRepository;
import workshift.database.Shop;
import workshift.database.ShopRepository;
import workshift.database.User;
import workshift.database.UserRepository;
import workshift.utilities.DateTimeRange;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.time.LocalDateTime;

/**
 * Test class to perform rest API integration tests on a real in-memory backed database.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = Application.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class RestApiTest
{
    @Test
    public void givenUser_whenGetUsers_thenStatus200() throws Exception
    {
        final User user = createUser("Test User");

        performGetRequestWithSuccess("/users")
                .andExpect(jsonPath("$.content.length()", is(1)))
                .andExpect(jsonPath("$.content[0].id", is(user.getId()), Long.class))
                .andExpect(jsonPath("$.content[0].name", is("Test User")));
    }

    @ParameterizedTest
    @ValueSource(strings = {"TestUser", "Test User", "@@", "\uD83D\uDE00"})
    public void whenCreateUser_thenStatus200(final String name) throws Exception
    {
        performPostRequestWithSuccess("/users?name={0}", name)
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andExpect(jsonPath("$.name", is(name)));
    }

    @ParameterizedTest
    @ValueSource(strings = {" ", "VeryLongNameVeryLongNameVeryLongNameVeryLongNameVeryLongName"})
    @EmptySource
    public void whenCreateUserWithInvalidName_thenStatus400(final String name) throws Exception
    {
        performPostRequestWithFailure("/users?name={0}", name)
                .andExpect(content().string("Name is invalid."));
    }

    @Test
    public void givenShop_whenGetShops_thenStatus200() throws Exception
    {
        final Shop shop = createShop("Test Shop");

        performGetRequestWithSuccess("/shops")
                .andExpect(jsonPath("$.content.length()", is(1)))
                .andExpect(jsonPath("$.content[0].id", is(shop.getId()), Long.class))
                .andExpect(jsonPath("$.content[0].name", is("Test Shop")));
    }

    @ParameterizedTest
    @ValueSource(strings = {"TestShop", "Test Shop", "@@", "\uD83D\uDE00"})
    public void whenCreateShop_thenStatus200(final String name) throws Exception
    {
        performPostRequestWithSuccess("/shops?name={0}", name)
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andExpect(jsonPath("$.name", is(name)));
    }

    @ParameterizedTest
    @ValueSource(strings = {" ", "VeryLongNameVeryLongNameVeryLongNameVeryLongNameVeryLongName"})
    @EmptySource
    public void whenCreateShopWithInvalidName_thenStatus400(final String name) throws Exception
    {
        performPostRequestWithFailure("/shops?name={0}", name)
                .andExpect(content().string("Name is invalid."));
    }

    @Test
    public void givenEmployment_whenGetEmployments_thenStatus200() throws Exception
    {
        final User user = createUser("Test User");
        final Shop shop = createShop("Test Shop");
        final Employment employment = createEmployment(user, shop);

        performGetRequestWithSuccess("/employments")
                .andExpect(jsonPath("$.content.length()", is(1)))
                .andExpect(jsonPath("$.content[0].id", is(employment.getId()), Long.class))
                .andExpect(jsonPath("$.content[0].user.id", is(user.getId()), Long.class))
                .andExpect(jsonPath("$.content[0].user.name", is("Test User")))
                .andExpect(jsonPath("$.content[0].shop.id", is(shop.getId()), Long.class))
                .andExpect(jsonPath("$.content[0].shop.name", is("Test Shop")));
    }

    @Test
    public void whenCreateEmployment_thenStatus200() throws Exception
    {
        final User user = createUser("Test User");
        final Shop shop = createShop("Test Shop");

        performPostRequestWithSuccess("/shops/{shop_id}/users/{user_id}", shop.getId(), user.getId())
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andExpect(jsonPath("$.user.id", is(user.getId()), Long.class))
                .andExpect(jsonPath("$.user.name", is("Test User")))
                .andExpect(jsonPath("$.shop.id", is(shop.getId()), Long.class))
                .andExpect(jsonPath("$.shop.name", is("Test Shop")));

        assertEquals(1, employmentRepository.count());
    }

    @Test
    public void whenCreateEmploymentWithInvalidShop_thenStatus400() throws Exception
    {
        final User user = createUser("Test User");
        performPostRequestWithFailure("/shops/1/users/{0}", user.getId())
                .andExpect(content().string("Shop id is not valid."));
    }

    @Test
    public void whenCreateEmploymentWithInvalidUser_thenStatus400() throws Exception
    {
        final Shop shop = createShop("Test Shop");
        performPostRequestWithFailure("/shops/{0}/users/1", shop.getId())
                .andExpect(content().string("User id is not valid."));
    }

    @Test
    public void givenShift_whenGetShifts_thenStatus200() throws Exception
    {
        final Shop shop = createShop("Test Shop");
        final LocalDateTime startTime = LocalDateTime.of(1995, 1, 1, 12, 0, 0);
        final LocalDateTime endTime = LocalDateTime.of(1995, 1, 1, 16, 0, 0);
        final Shift shift = createShift(shop, startTime, endTime, null);

        performGetRequestWithSuccess("/shifts")
                .andExpect(jsonPath("$.content.length()", is(1)))
                .andExpect(jsonPath("$.content[0].id", is(shift.getId()), Long.class))
                .andExpect(jsonPath("$.content[0].shop.id", is(shop.getId()), Long.class))
                .andExpect(jsonPath("$.content[0].shop.name", is("Test Shop")))
                .andExpect(jsonPath("$.content[0].time.startTime", is("1995-01-01T12:00:00")))
                .andExpect(jsonPath("$.content[0].time.endTime", is("1995-01-01T16:00:00")));
    }

    @Test
    public void whenCreateShift_thenStatus200() throws Exception
    {
        final Shop shop = createShop("Test Shop");

        performPostRequestWithSuccess("/shops/{shop_id}/shifts?startTime=1995-01-01T12:01:02&endTime=1995-01-01T16:30:30", shop.getId())
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andExpect(jsonPath("$.shop.id", is(shop.getId()), Long.class))
                .andExpect(jsonPath("$.shop.name", is("Test Shop")))
                .andExpect(jsonPath("$.time.startTime", is("1995-01-01T12:01:02")))
                .andExpect(jsonPath("$.time.endTime", is("1995-01-01T16:30:30")));

        assertEquals(1, shiftRepository.count());
    }

    @Test
    public void whenCreateShiftWithInvalidShop_thenStatus400() throws Exception
    {
        performPostRequestWithFailure("/shops/5/shifts?startTime=1995-01-01T12:00:00&endTime=1995-01-01T16:00:00")
                .andExpect(content().string("Shop id is not valid."));
    }

    @Test
    public void whenCreateShiftWithInvalidStartTime_thenStatus400() throws Exception
    {
        final Shop shop = createShop("Test Shop");

        performPostRequestWithFailure("/shops/{0}/shifts?startTime=bob&endTime=1995-01-01T16:00:00", shop.getId())
                .andExpect(content().string("Invalid date format."));
    }

    @Test
    public void whenCreateShiftWithInvalidEndTime_thenStatus400() throws Exception
    {
        final Shop shop = createShop("Test Shop");

        performPostRequestWithFailure("/shops/{0}/shifts?startTime=1995-01-01T16:00:00&endTime=-1212", shop.getId())
                .andExpect(content().string("Invalid date format."));
    }

    @Test
    public void givenShiftAssignment_whenAssignToUser_thenStatus200() throws Exception
    {
        final User user = createUser("Test User");
        final Shop shop = createShop("Test Shop");
        createEmployment(user, shop);
        final Shift shift = createShiftBetweenHours(shop, 12, 18, null);

        performPatchRequestWithSuccess("/shifts/{0}/users/{1}", shift.getId(), user.getId())
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andExpect(jsonPath("$.shop.id", is(shop.getId()), Long.class))
                .andExpect(jsonPath("$.user.id", is(user.getId()), Long.class));

        assertEquals(1, shiftRepository.count());
    }

    @Test
    public void givenShiftAssignment_whenAssignToInvalidShift_thenStatus400() throws Exception
    {
        final User user = createUser("Test User");

        performPatchRequestWithFailure("/shifts/1/users/{0}", user.getId())
                .andExpect(content().string("Shift id is not valid."));
    }

    @Test
    public void givenShiftAssignment_whenAssignToInvalidUser_thenStatus400() throws Exception
    {
        final Shop shop = createShop("Test Shop");
        final Shift shift = createShiftBetweenHours(shop, 1, 20, null);

        performPatchRequestWithFailure("/shifts/{0}/users/1", shift.getId())
                .andExpect(content().string("User id is not valid."));
    }

    @Test
    public void givenShiftAssignment_whenAssignToNonEmployedUser_thenStatus400() throws Exception
    {
        final User user = createUser("Test User");
        final Shop shop = createShop("Test Shop");
        final Shift shift = createShiftBetweenHours(shop, 12, 16, null);

        performPatchRequestWithFailure("/shifts/{0}/users/{1}", shift.getId(), user.getId())
                .andExpect(content().string("User is not employed in the shop."));
    }

    @Test
    public void givenShiftAssignment_whenAssignedToAnotherShopAtTheSameTime_thenStatus400() throws Exception
    {
        final User user = createUser("Test User");

        final Shop shop1 = createShop("Test Shop 1");
        createEmployment(user, shop1);
        final Shift shiftForShop1 = createShiftBetweenHours(shop1, 12, 16, user);
        assertNotNull(shiftForShop1);

        final Shop shop2 = createShop("Test Shop 2");
        createEmployment(user, shop2);
        final Shift shiftForShop2 = createShiftBetweenHours(shop2, 13, 18, null);
        assertNotNull(shiftForShop2);

        performPatchRequestWithFailure("/shifts/{0}/users/{1}", shiftForShop2.getId(), user.getId())
                .andExpect(content().string("Cannot work in multiple shops at the same time."));
    }

    @Test
    public void givenShiftAssignment_whenAssignedMoreThanFiveDaysInSameShop_thenStatus400() throws Exception
    {
        final User user = createUser("Test User");
        final Shop shop = createShop("Test Shop");
        createEmployment(user, shop);

        createShiftForDay(shop, 1, user);
        createShiftForDay(shop, 2, user);
        createShiftForDay(shop, 3, user);
        createShiftForDay(shop, 4, user);
        createShiftForDay(shop, 5, user);
        final Shift shiftForDay6 = createShiftForDay(shop, 6, null);

        performPatchRequestWithFailure("/shifts/{0}/users/{1}", shiftForDay6.getId(), user.getId())
                .andExpect(content().string("Cannot work in the same shop 5 days in a row."));
    }

    @Test
    public void givenShiftAssignment_whenAssignedMoreThan8HoursInSameShopWithin24Hours_thenStatus400() throws Exception
    {
        final User user = createUser("Test User");
        final Shop shop = createShop("Test Shop");
        createEmployment(user, shop);

        final Shift shift1 = createShiftBetweenHours(shop, 2, 8, user);
        assertNotNull(shift1);
        final Shift shift2 = createShiftBetweenHours(shop, 10, 18, null);
        assertNotNull(shift2);

        performPatchRequestWithFailure("/shifts/{0}/users/{1}", shift2.getId(), user.getId())
                .andExpect(content().string("Cannot work for more than 8 hours in the same shop."));
    }

    /**
     * Clear all repositories for the next test.
     */
    @AfterEach
    public void afterEachTest()
    {
        shiftRepository.deleteAll();
        shiftRepository.flush();
        employmentRepository.deleteAll();
        employmentRepository.flush();
        userRepository.deleteAll();
        userRepository.flush();
        shopRepository.deleteAll();
        shopRepository.flush();
    }

    /**
     * Helper functions to perform get and post requests.
     */
    private ResultActions performGetRequestWithSuccess(final String url, final Object... urlVariables) throws Exception
    {
        return mvc.perform(get(url, urlVariables))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
    private ResultActions performPostRequestWithSuccess(final String url, final Object... urlVariables) throws Exception
    {
        return mvc.perform(post(url, urlVariables))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
    private ResultActions performPostRequestWithFailure(final String url, final Object... urlVariables) throws Exception
    {
        return mvc.perform(post(url, urlVariables))
                .andExpect(status().is(400));
    }
    private ResultActions performPatchRequestWithSuccess(final String url, final Object... urlVariables) throws Exception
    {
        return mvc.perform(patch(url, urlVariables))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }
    private ResultActions performPatchRequestWithFailure(final String url, final Object... urlVariables) throws Exception
    {
        return mvc.perform(patch(url, urlVariables))
                .andExpect(status().is(400));
    }

    /**
     * Helper functions to create different things.
     */
    private User createUser(final String Name)
    {
        final User user = new User(Name);
        userRepository.saveAndFlush(user);
        return user;
    }
    private Shop createShop(final String Name)
    {
        final Shop shop = new Shop(Name);
        shopRepository.saveAndFlush(shop);
        return shop;
    }
    private Employment createEmployment(final User user, final Shop shop)
    {
        final Employment employment = new Employment(user, shop);
        employmentRepository.saveAndFlush(employment);
        return employment;
    }

    /**
     * Helper functions to create different shifts.
     */
    private Shift createShift(final Shop shop, final LocalDateTime startTime, final LocalDateTime endTime, final User user)
    {
        final Shift shift = new Shift(shop, new DateTimeRange(startTime, endTime));
        shift.setUser(user);
        shiftRepository.saveAndFlush(shift);
        return shift;
    }
    private Shift createShiftBetweenHours(final Shop shop, final int startHour, final int endHour, final User user)
    {
        final LocalDateTime referenceTime = LocalDateTime.of(2020, 1, 1, 0, 0, 0);
        final LocalDateTime startTime = referenceTime.withHour(startHour);
        final LocalDateTime endTime = referenceTime.withHour(endHour);
        return createShift(shop, startTime, endTime, user);
    }
    private Shift createShiftForDay(final Shop shop, final int shiftDay, final User user)
    {
        final LocalDateTime referenceTime = LocalDateTime.of(1995, 1, shiftDay, 0, 0, 0);
        final LocalDateTime startTime = referenceTime.withHour(12);
        final LocalDateTime endTime = referenceTime.withHour(16);
        return createShift(shop, startTime, endTime, user);
    }

    /**
     * The repositories are autoconfigured from the test database.
     */
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ShopRepository shopRepository;
    @Autowired
    private EmploymentRepository employmentRepository;
    @Autowired
    private ShiftRepository shiftRepository;
    @Autowired
    private MockMvc mvc;
}