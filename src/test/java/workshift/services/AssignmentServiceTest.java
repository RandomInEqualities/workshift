package workshift.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import workshift.database.EmploymentRepository;
import workshift.database.Shift;
import workshift.database.ShiftRepository;
import workshift.database.Shop;
import workshift.database.User;
import workshift.exceptions.ServiceException;
import workshift.utilities.DateTimeRange;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Test class to add unit tests for the assignment service.
 */
@ExtendWith(MockitoExtension.class)
class AssignmentServiceTest
{
    @Test
    void assignUserToShift_whenAssignmentIsSuccessful_thenSaveToRepository()
    {
        final User user = new User("Test User");
        final Shop shop = new Shop("Test Shop");
        when(employmentRepository.countEmploymentsByUserInShop(user.getId(), shop.getId())).thenReturn(1L);
        when(shiftRepository.countShiftsByUserWithinTime(eq(user.getId()), any(), any(), eq(shop.getId()))).thenReturn(0L);
        when(shiftRepository.getShiftTimesByUserWithinTime(eq(user.getId()), any(), any())).thenReturn(new ArrayList<>());
        when(shiftRepository.getShiftTimesByUserAndShopWithinTime(eq(user.getId()), eq(shop.getId()), any(), any())).thenReturn(new ArrayList<>());

        final Shift shift = new Shift(shop, createTimeBetweenHours(12, 16));
        assignmentService.assignUserToShift(shift, user);
        verify(shiftRepository).save(shiftCaptor.capture());
        assertEquals(shift, shiftCaptor.getValue());
        assertEquals(user, shiftCaptor.getValue().getUser());
        verifyNoMoreInteractions(shiftRepository, employmentRepository);
    }

    @Test
    void assignUserToShift_whenUserNotEmployedInShop_thenExceptionThrown()
    {
        final User user = new User("Test User");
        final Shop shop = new Shop("Test Shop");
        when(employmentRepository.countEmploymentsByUserInShop(user.getId(), shop.getId())).thenReturn(0L);

        final Shift shift = new Shift(shop, createTimeBetweenHours(12, 16));
        final var exception = assertThrows(ServiceException.class, () -> assignmentService.assignUserToShift(shift, user));
        assertEquals("User is not employed in the shop.", exception.getMessage());
        verifyNoMoreInteractions(shiftRepository, employmentRepository);
    }

    @Test
    void assignUserToShift_whenUserWorksInMultipleShops_thenExceptionThrown()
    {
        final User user = new User("Test User");
        final Shop shop = new Shop("Test Shop");
        when(employmentRepository.countEmploymentsByUserInShop(user.getId(), shop.getId())).thenReturn(1L);
        when(shiftRepository.countShiftsByUserWithinTime(eq(user.getId()), any(), any(), eq(shop.getId()))).thenReturn(1L);

        final Shift shift = new Shift(shop, createTimeBetweenHours(12, 16));
        final var exception = assertThrows(ServiceException.class, () -> assignmentService.assignUserToShift(shift, user));
        assertEquals("Cannot work in multiple shops at the same time.", exception.getMessage());
        verifyNoMoreInteractions(shiftRepository, employmentRepository);
    }

    @Test
    void assignUserToShift_whenUserGetsTooManyConsecutiveDays_thenExceptionThrown()
    {
        final User user = new User("Test User");
        final Shop shop = new Shop("Test Shop");
        when(employmentRepository.countEmploymentsByUserInShop(user.getId(), shop.getId())).thenReturn(1L);
        when(shiftRepository.countShiftsByUserWithinTime(eq(user.getId()), any(), any(), eq(shop.getId()))).thenReturn(0L);

        final List<DateTimeRange> existingShiftTimes = new ArrayList<>();
        existingShiftTimes.add(createTimeRangeForDay(1));
        existingShiftTimes.add(createTimeRangeForDay(2));
        existingShiftTimes.add(createTimeRangeForDay(3));
        existingShiftTimes.add(createTimeRangeForDay(4));
        existingShiftTimes.add(createTimeRangeForDay(5));
        when(shiftRepository.getShiftTimesByUserAndShopWithinTime(eq(user.getId()), eq(shop.getId()), any(), any())).thenReturn(existingShiftTimes);

        final Shift shift = new Shift(shop, createTimeRangeForDay(6));
        final var exception = assertThrows(ServiceException.class, () -> assignmentService.assignUserToShift(shift, user));
        assertEquals("Cannot work in the same shop 5 days in a row.", exception.getMessage());
        verifyNoMoreInteractions(shiftRepository, employmentRepository);
    }

    @Test
    void assignUserToShift_whenUserGetsTooManyConsecutiveWorkHours_thenExceptionThrown()
    {
        final User user = new User("Test User");
        final Shop shop = new Shop("Test Shop");
        final List<DateTimeRange> existingShiftTimes = new ArrayList<>(List.of(createTimeBetweenHours(0, 4)));
        when(employmentRepository.countEmploymentsByUserInShop(user.getId(), shop.getId())).thenReturn(1L);
        when(shiftRepository.countShiftsByUserWithinTime(eq(user.getId()), any(), any(), eq(shop.getId()))).thenReturn(0L);
        when(shiftRepository.getShiftTimesByUserAndShopWithinTime(eq(user.getId()), eq(shop.getId()), any(), any())).thenReturn(new ArrayList<>());
        when(shiftRepository.getShiftTimesByUserWithinTime(eq(user.getId()), any(), any())).thenReturn(existingShiftTimes);

        final Shift shift = new Shift(shop, createTimeBetweenHours(6, 12));
        final var exception = assertThrows(ServiceException.class, () -> assignmentService.assignUserToShift(shift, user));
        assertEquals("Cannot work for more than 8 hours in the same shop.", exception.getMessage());
        verifyNoMoreInteractions(shiftRepository, employmentRepository);
    }

    private DateTimeRange createTimeRangeForDay(final int day)
    {
        final LocalDateTime startTime = LocalDateTime.of(2035, 10, day, 10, 0);
        final LocalDateTime endTime = LocalDateTime.of(2035, 10, day, 14, 0);
        return new DateTimeRange(startTime, endTime);
    }

    private DateTimeRange createTimeBetweenHours(final int startHour, final int endHour)
    {
        final LocalDateTime startTime = LocalDateTime.of(2035, 10, 15, startHour, 0);
        final LocalDateTime endTime = LocalDateTime.of(2035, 10, 15, endHour, 0);
        return new DateTimeRange(startTime, endTime);
    }

    @InjectMocks
    private AssignmentService assignmentService;
    @Mock
    private EmploymentRepository employmentRepository;
    @Mock
    private ShiftRepository shiftRepository;
    @Captor
    private ArgumentCaptor<Shift> shiftCaptor;
}