package workshift.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import workshift.database.Employment;
import workshift.database.EmploymentRepository;
import workshift.database.Shift;
import workshift.database.ShiftRepository;
import workshift.database.Shop;
import workshift.database.ShopRepository;
import workshift.database.User;
import workshift.database.UserRepository;
import workshift.exceptions.ServiceException;
import workshift.utilities.DateTimeRange;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import java.time.LocalDateTime;

/**
 * Test class to add unit tests for the creation service.
 */
@ExtendWith(MockitoExtension.class)
public class CreationServiceTest
{
    @Test
    public void createUser_whenCreationIsSuccessful_thenSaveToRepository()
    {
        creationService.createUser("Test User");
        verify(userRepository).save(userCaptor.capture());
        assertEquals("Test User", userCaptor.getValue().getName());
        verifyNoMoreInteractions(userRepository, shopRepository, employmentRepository, shiftRepository);
    }

    @Test
    public void createUser_whenCreateUserWithInvalidName_thenExceptionThrown()
    {
        final var exception = assertThrows(ServiceException.class, () -> creationService.createUser(" "));
        assertEquals("Name is invalid.", exception.getMessage());
        verifyNoInteractions(userRepository, shopRepository, employmentRepository, shiftRepository);
    }

    @Test
    public void createShop_whenCreationIsSuccessful_thenSaveToRepository()
    {
        creationService.createShop("Test Shop");
        verify(shopRepository).save(shopCaptor.capture());
        assertEquals("Test Shop", shopCaptor.getValue().getName());
        verifyNoMoreInteractions(userRepository, shopRepository, employmentRepository, shiftRepository);
    }

    @Test
    public void createShop_whenCreateShopWithInvalidName_thenExceptionThrown()
    {
        final var exception = assertThrows(ServiceException.class, () -> creationService.createShop(" "));
        assertEquals("Name is invalid.", exception.getMessage());
        verifyNoInteractions(userRepository, shopRepository, employmentRepository, shiftRepository);
    }

    @Test
    public void createEmployment_whenCreationIsSuccessful_thenSaveToRepository()
    {
        final User user = new User("Test User");
        final Shop shop = new Shop("Test Shop");
        when(employmentRepository.countEmploymentsByUserInShop(user.getId(), shop.getId())).thenReturn(0L);

        creationService.createEmployment(user, shop);
        verify(employmentRepository).save(employmentCaptor.capture());
        assertEquals(user, employmentCaptor.getValue().getUser());
        assertEquals(shop, employmentCaptor.getValue().getShop());
        verifyNoMoreInteractions(userRepository, shopRepository, employmentRepository, shiftRepository);
    }

    @Test
    public void createEmployment_whenCreateEmploymentWhenThereIsExistingEmployment_thenExceptionThrown()
    {
        final User user = new User("Test User");
        final Shop shop = new Shop("Test Shop");
        when(employmentRepository.countEmploymentsByUserInShop(user.getId(), shop.getId())).thenReturn(1L);

        final var exception = assertThrows(ServiceException.class, () -> creationService.createEmployment(user, shop));
        assertEquals("User is already employed in shop.", exception.getMessage());
        verifyNoMoreInteractions(userRepository, shopRepository, employmentRepository, shiftRepository);
    }

    @Test
    public void createShift_whenCreationIsSuccessful_thenSaveToRepository()
    {
        final Shop shop = new Shop("Test Shop");
        final DateTimeRange shiftTime = createTimeBetweenHours(0, 4);
        creationService.createShift(shop, shiftTime);
        verify(shiftRepository).save(shiftCaptor.capture());
        assertEquals(shop, shiftCaptor.getValue().getShop());
        assertEquals(shiftTime, shiftCaptor.getValue().getTime());
        assertNull(shiftCaptor.getValue().getUser());
        verifyNoMoreInteractions(userRepository, shopRepository, employmentRepository, shiftRepository);
    }

    @Test
    public void createShift_whenCreateShiftWithEndTimeBeforeStartTime_thenExceptionThrown()
    {
        final Shop shop = new Shop("Test Shop");
        final DateTimeRange shiftTime = createTimeBetweenHours(4, 0);
        final var exception = assertThrows(ServiceException.class, () -> creationService.createShift(shop, shiftTime));
        assertEquals("End time cannot be before start time.", exception.getMessage());
        verifyNoInteractions(userRepository, shopRepository, employmentRepository, shiftRepository);
    }

    @Test
    public void createShift_whenCreateShiftWithLongerDurationThanAllowed_thenExceptionThrown()
    {
        final Shop shop = new Shop("Test Shop");
        final DateTimeRange shiftTime = createTimeBetweenHours(0, 10);
        final var exception = assertThrows(ServiceException.class, () -> creationService.createShift(shop, shiftTime));
        assertEquals("It is not allowed to have shifts longer than 8 hours.", exception.getMessage());
        verifyNoInteractions(userRepository, shopRepository, employmentRepository, shiftRepository);
    }

    @ParameterizedTest
    @ValueSource(strings = {"User", "Test User", "@@", "\uD83D\uDE00"})
    public void isAppropriateShopOrUserName_whenNameIsValid_thenReturnsTrue(final String name)
    {
        assertTrue(CreationService.isAppropriateShopOrUserName(name));
    }

    @ParameterizedTest
    @ValueSource(strings = {" ", " \n\r", "VeryLongNameVeryLongNameVeryLongNameVeryLongNameVeryLongName"})
    @NullAndEmptySource
    public void isAppropriateShopOrUserName_whenNameIsInvalid_thenReturnsFalse(final String name)
    {
        assertFalse(CreationService.isAppropriateShopOrUserName(name));
    }

    private DateTimeRange createTimeBetweenHours(final int startHour, final int endHour)
    {
        final LocalDateTime startTime = LocalDateTime.of(2035, 10, 15, startHour, 0);
        final LocalDateTime endTime = LocalDateTime.of(2035, 10, 15, endHour, 0);
        return new DateTimeRange(startTime, endTime);
    }

    @InjectMocks
    private CreationService creationService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private ShopRepository shopRepository;
    @Mock
    private EmploymentRepository employmentRepository;
    @Mock
    private ShiftRepository shiftRepository;

    @Captor
    private ArgumentCaptor<User> userCaptor;
    @Captor
    private ArgumentCaptor<Shop> shopCaptor;
    @Captor
    private ArgumentCaptor<Employment> employmentCaptor;
    @Captor
    private ArgumentCaptor<Shift> shiftCaptor;
}