package workshift.utilities;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static workshift.utilities.TimeUtilities.findLongestConsecutiveDaysInRanges;
import static workshift.utilities.TimeUtilities.findLongestDurationWithinMovingWindow;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Test class to add unit tests for time utilities.
 */
public class TimeUtilitiesTest
{
    @Test
    public void findLongestConsecutiveDaysInRanges_whenThereIsSingleConsecutiveDay_thenCorrectTime()
    {
        final List<DateTimeRange> ranges = new ArrayList<>();
        ranges.add(new DateTimeRange(referenceTime.withDayOfMonth(5).withHour(6), referenceTime.withDayOfMonth(5).withHour(12)));
        ranges.add(new DateTimeRange(referenceTime.withDayOfMonth(7).withHour(6), referenceTime.withDayOfMonth(7).withHour(12)));
        final LocalDate searchStartDate = referenceTime.minusMonths(1).toLocalDate();
        final LocalDate searchEndDate = referenceTime.plusMonths(1).toLocalDate();
        assertEquals(1, findLongestConsecutiveDaysInRanges(ranges, searchStartDate, searchEndDate));
    }

    @Test
    public void findLongestConsecutiveDaysInRanges_whenThereIsMultipleConsecutiveDays_thenCorrectTime()
    {
        final List<DateTimeRange> ranges = new ArrayList<>();
        ranges.add(new DateTimeRange(referenceTime.withDayOfMonth(5), referenceTime.withDayOfMonth(6)));
        ranges.add(new DateTimeRange(referenceTime.withDayOfMonth(7), referenceTime.withDayOfMonth(8)));
        final LocalDate searchStartDate = referenceTime.minusMonths(1).toLocalDate();
        final LocalDate searchEndDate = referenceTime.plusMonths(1).toLocalDate();
        assertEquals(4, findLongestConsecutiveDaysInRanges(ranges, searchStartDate, searchEndDate));
    }

    @Test
    public void findLongestConsecutiveDaysInRanges_whenThereIsNoRangesInSearchInterval_thenZeroTime()
    {
        final List<DateTimeRange> ranges = List.of(new DateTimeRange(referenceTime.withDayOfMonth(5), referenceTime.withDayOfMonth(6)));
        final LocalDate searchStartDate = referenceTime.plusMonths(5).toLocalDate();
        final LocalDate searchEndDate = referenceTime.plusMonths(6).toLocalDate();
        assertEquals(0, findLongestConsecutiveDaysInRanges(ranges, searchStartDate, searchEndDate));
    }

    @Test
    public void findLongestDurationWithinMovingWindow_whenThereIsSingleRangeOverlap_thenCorrectTime()
    {
        final List<DateTimeRange> ranges = new ArrayList<>();
        ranges.add(new DateTimeRange(referenceTime.plusHours(0), referenceTime.plusHours(4)));
        ranges.add(new DateTimeRange(referenceTime.plusHours(12), referenceTime.plusHours(16)));
        final Duration timeWindow = Duration.ofHours(6);
        assertEquals(Duration.ofHours(4), findLongestDurationWithinMovingWindow(ranges, timeWindow));
    }

    @Test
    public void findLongestDurationWithinMovingWindow_whenThereIsMultipleRangeOverlap_thenCorrectTime()
    {
        final List<DateTimeRange> ranges = new ArrayList<>();
        ranges.add(new DateTimeRange(referenceTime.plusHours(0), referenceTime.plusHours(4)));
        ranges.add(new DateTimeRange(referenceTime.plusHours(6), referenceTime.plusHours(12)));
        final Duration timeWindow = Duration.ofHours(6);
        assertEquals(Duration.ofHours(6), findLongestDurationWithinMovingWindow(ranges, timeWindow));
    }

    @Test
    public void findLongestDurationWithinMovingWindow_whenThereIsTwoOverlappingRanges_thenCorrectTime()
    {
        final List<DateTimeRange> ranges = new ArrayList<>();
        ranges.add(new DateTimeRange(referenceTime.plusHours(0), referenceTime.plusHours(4)));
        ranges.add(new DateTimeRange(referenceTime.plusHours(2), referenceTime.plusHours(5)));
        final Duration timeWindow = Duration.ofHours(6);
        assertEquals(Duration.ofHours(5), findLongestDurationWithinMovingWindow(ranges, timeWindow));
    }

    @Test
    public void findLongestDurationWithinMovingWindow_whenThereIsMultipleOverlappingRanges_thenCorrectTime()
    {
        final List<DateTimeRange> ranges = new ArrayList<>();
        ranges.add(new DateTimeRange(referenceTime.plusHours(0), referenceTime.plusHours(4)));
        ranges.add(new DateTimeRange(referenceTime.plusHours(6), referenceTime.plusHours(12)));
        ranges.add(new DateTimeRange(referenceTime.plusHours(22), referenceTime.plusHours(24)));
        final Duration timeWindow = Duration.ofHours(24);
        assertEquals(Duration.ofHours(12), findLongestDurationWithinMovingWindow(ranges, timeWindow));
    }

    @Test
    public void findLongestDurationWithinMovingWindow_whenThereIsUnsortedData_thenCorrectTime()
    {
        final List<DateTimeRange> ranges = new ArrayList<>();
        ranges.add(new DateTimeRange(referenceTime.plusHours(10), referenceTime.plusHours(17)));
        ranges.add(new DateTimeRange(referenceTime.plusHours(0), referenceTime.plusHours(2)));
        ranges.add(new DateTimeRange(referenceTime.plusHours(7), referenceTime.plusHours(14)));
        final Duration timeWindow = Duration.ofHours(14);
        assertEquals(Duration.ofHours(10), findLongestDurationWithinMovingWindow(ranges, timeWindow));
    }

    @Test
    public void findLongestDurationWithinMovingWindow_whenOneRangeFullyCoversAnotherRange_thenCorrectTime()
    {
        final List<DateTimeRange> ranges = new ArrayList<>();
        ranges.add(new DateTimeRange(referenceTime.plusHours(4), referenceTime.plusHours(10)));
        ranges.add(new DateTimeRange(referenceTime.plusHours(0), referenceTime.plusHours(14)));
        ranges.add(new DateTimeRange(referenceTime.plusHours(30), referenceTime.plusHours(45)));
        final Duration timeWindow = Duration.ofHours(20);
        assertEquals(Duration.ofHours(15), findLongestDurationWithinMovingWindow(ranges, timeWindow));
    }

    @Test
    public void findLongestDurationWithinMovingWindow_whenThereIsNoData_thenZeroTime()
    {
        final Duration timeWindow = Duration.ofHours(1);
        assertEquals(Duration.ofHours(0), findLongestDurationWithinMovingWindow(Collections.emptyList(), timeWindow));
    }

    private final LocalDateTime referenceTime = LocalDateTime.of(2000, 5, 1, 12, 0);
}
